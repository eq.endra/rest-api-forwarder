/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package id.co.id.rcs.http.server;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.KeyStore;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManagerFactory;
import com.sun.net.httpserver.*;
import com.sun.net.httpserver.HttpsServer;
import id.co.id.rcs.handler.MyHandlerHttp;
import id.co.id.rcs.utils.MyUtils;
import org.andrejs.json.Json;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Main {
    private static final Logger log    = LoggerFactory.getLogger(Main.class);
    public static Json          config;

    /**
     * @param args
     */
    public static void main(String[] args) {
        final Main    main             = new Main();
        final MyUtils myUtils          = new MyUtils();
        final String  proxyyml         = "./config/proxyurls.yml";
        config                         = myUtils.loadConfig(proxyyml);

        final Json backendpaths        = config.at("proxyurls");
        final Json serverConfig        = config.at("server");

        //        final Map<String, Object> map                  = backendpaths.toMap();
        final String host = serverConfig.get("host", "0.0.0.0");
        final int    port = serverConfig.get("port", 8000);
        //        final int                 maxRequestTime       = serverConfig.get("maxRequestTime", 30000);
        //        final int                 ioThreads            = serverConfig.get("ioThread", 4);
        //        final int                 workerThreads        = serverConfig.get("workerThreads", Runtime.getRuntime().availableProcessors() * 8);
        //        final int                 workerTaskMaxThreads = serverConfig.get("workerTaskMaxThreads", workerThreads);
        //        final int                 backlog              = serverConfig.get("backlog", 1000);
        main.initHttp(port);

        if (serverConfig.containsKey("sslPort")) {
            final int    sslPort          = serverConfig.get("sslPort", 443);
            final String keystorePassword = serverConfig.get("keystorePassword", "");
            final String ketstoreFile     = serverConfig.get("keystore", "");
            main.initHttps(sslPort, keystorePassword, ketstoreFile);
        }
    }

    private void initHttp(final int port) {
        try {
            //            final int        port   = 9000;
            final HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);

            server.createContext("/", new MyHandlerHttp());
            server.setExecutor(new ThreadPoolExecutor(4, 8, 30, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(100)));

            server.start();
            log.info("HTTP server on port : " + port);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    private void initHttps(final int port, final String keystorePassword, final String keystoreFile) {
        try {
            // setup the socket address
            final InetSocketAddress address = new InetSocketAddress(port);

            // initialise the HTTPS server
            final HttpsServer httpsServer = HttpsServer.create(address, 0);
            final SSLContext  sslContext  = SSLContext.getInstance("TLS");

            // initialise the keystore
            final char[]   password = keystorePassword.toCharArray();
            final KeyStore ks       = KeyStore.getInstance("JKS");

            //            final String keystoreFile = "./certificate/testkey.jks";
            final FileInputStream fis = new FileInputStream(keystoreFile);
            ks.load(fis, password);

            // setup the key manager factory
            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            kmf.init(ks, password);

            // setup the trust manager factory
            TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
            tmf.init(ks);

            // setup the HTTPS context and parameters
            sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
            httpsServer.setHttpsConfigurator(new HttpsConfigurator(sslContext) {
                    public void configure(HttpsParameters params) {
                        try {
                            // initialise the SSL context
                            SSLContext context = getSSLContext();
                            SSLEngine  engine  = context.createSSLEngine();
                            params.setNeedClientAuth(false);
                            params.setCipherSuites(engine.getEnabledCipherSuites());
                            params.setProtocols(engine.getEnabledProtocols());

                            // Set the SSL parameters
                            SSLParameters sslParameters = context.getSupportedSSLParameters();
                            params.setSSLParameters(sslParameters);
                        } catch (Exception ex) {
                            System.out.println("Failed to create HTTPS port");
                        }
                    }
                });
            httpsServer.createContext("/", new MyHandlerHttp());
            // default
            //            httpsServer.setExecutor(null); // creates a default executor
            //
            // threadpool
            httpsServer.setExecutor(new ThreadPoolExecutor(4, 8, 30, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(100)));
            httpsServer.start();

            log.info("HTTPS server on port : " + port);
        } catch (Exception exception) {
            log.error("Failed to create HTTPS server on port " + port + " of localhost", exception);

            //            exception.printStackTrace();
        }
    }
}
