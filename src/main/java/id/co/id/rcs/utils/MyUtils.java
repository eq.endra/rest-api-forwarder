/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package id.co.id.rcs.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import org.andrejs.json.Json;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;


/**
 *
 * @author mikomkz
 */
public class MyUtils {
    private static final Logger log = LoggerFactory.getLogger(MyUtils.class);

    public Json loadConfig(final String proxyyml) {
        //        Properties props = new Properties();
        final Yaml yml = new Yaml();

        //        final String proxyyml = "proxy.yml";
        Path                ymlfile = Paths.get(proxyyml);
        Map<String, Object> config;

        if (Files.exists(ymlfile)) {
            try {
                final InputStream is = Files.newInputStream(ymlfile);
                config              = (Map<String, Object>) yml.load(is);
                is.close();
            } catch (IOException e) {
                log.error("Could not load " + ymlfile.toAbsolutePath(), e);
                throw new IOError(e);
            }

            log.info("Loaded config from " + ymlfile.toAbsolutePath() + ": " + config);
        } else {
            ClassLoader classLoader = MyUtils.class.getClassLoader();
            config = (Map<String, Object>) yml.load(classLoader.getResourceAsStream(proxyyml));
            log.info("Loaded config from classpath proxy.yml: " + config);
        }

        //        flatten(config, props, "");
        return new Json(config);
    }

    public String getString(InputStream inputStream) {
        String returnValue = "";

        try {
            final StringBuilder sb = new StringBuilder();

            for (int ch; (ch = inputStream.read()) != -1;) {
                if (ch == 255) {
                    break;
                }

                final char name = (char) ch;
                sb.append(name);
            }

            returnValue = sb.toString();

            //            inputStream.close();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        // StandardCharsets.UTF_8.name() > JDK 7
        return returnValue;
    }

    public String sendData(final String ip, final int port, final String tMessageStream) {
        //        final String            ip                = SystemConfig.getNameSpace("pemda-pushdata").getStringParameter("sp2d-pemda-service-ip", "127.0.0.1");
        //        final int               port              = SystemConfig.getNameSpace("pemda-pushdata").getIntParameter("sp2d-pemda-service-port", 11123);
        final InetSocketAddress inetSocketAddress = new InetSocketAddress(ip, port);
        String                  tResponseStream   = "";

        log.info("SOCKET_REQUEST to " + inetSocketAddress + " : " + tMessageStream);

        try {
            final Socket tGatewaySocket = new Socket();

            tGatewaySocket.setSoTimeout(120000);
            tGatewaySocket.connect(inetSocketAddress, 60000);

            final ByteArrayOutputStream tRequestByteStream = new ByteArrayOutputStream();

            tRequestByteStream.write(tMessageStream.getBytes());
            tRequestByteStream.write(-1);
            tGatewaySocket.getOutputStream().write(tRequestByteStream.toByteArray());

            byte                tMessageByte = -1;
            final StringBuilder sb           = new StringBuilder();
            final InputStream   tInputStream = tGatewaySocket.getInputStream();

            while ((tMessageByte = (byte) tInputStream.read()) != -1) {
                sb.append((char) tMessageByte);
            }

            tResponseStream = sb.toString();
            tInputStream.close();
            tRequestByteStream.close();
            tGatewaySocket.close();
        } catch (IOException e) {
            // set timeout
            tResponseStream = "{\"rc\":\"0068\",\"rcm\":\"SOCKET NO CONNECTION\"}";
        }

        log.info("SOCKET_RESPONSE from " + inetSocketAddress + " : " + tResponseStream);

        //log.info(pPType);
        return tResponseStream;
    }
}
