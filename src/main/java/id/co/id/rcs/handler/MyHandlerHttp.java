/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package id.co.id.rcs.handler;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.util.Map;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import static id.co.id.rcs.http.server.Main.config;
import id.co.id.rcs.utils.MyUtils;
import org.andrejs.json.Json;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author mikomkz
 */
public class MyHandlerHttp implements HttpHandler {
    private static final Logger log = LoggerFactory.getLogger(MyHandlerHttp.class);

    @Override
    public void handle(HttpExchange t) {
        try {
            final MyUtils myUtils = new MyUtils();

            // request
            final String              request      = myUtils.getString(t.getRequestBody());
            final String              requestURI   = t.getRequestURI().getPath();
            final Json                backendpaths = config.at("proxyurls");
            final Map<String, Object> map          = backendpaths.toMap();

            log.info("HTTP_REQUEST {} : {} ", requestURI, request);

            String response = "response";

            if (requestURI.equals("/v1/api/check/trx/fds")) {
                response = "{\"RC\":\"44444\",\"RM\":\"API CHECK TRX TO FDS BELUM DIIMPLEMENTASIKAN\"}";
            } else if (map.containsKey(requestURI)) {
                final String dest = (String) map.get(requestURI);
                log.info("DEST_REQUEST {} : {} ", dest, request);

                if (dest.startsWith("http")) {
                    // TODO :: http type
                } else {
                    final String[] split = dest.split(":");
                    final String   ip    = split[0].trim();
                    final int      port  = Integer.parseInt(split[1].trim());
                    response             = myUtils.sendData(ip, port, request);
                }

                log.info("DEST_RESPONSE {} : {} ", dest, response);
            }

            // response
            t.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
            t.getResponseHeaders().add("Content-Type", "application/json");
            t.sendResponseHeaders(200, response.length());

            final OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        t.close();
    }
}
