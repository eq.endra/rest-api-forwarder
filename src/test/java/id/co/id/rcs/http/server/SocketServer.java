/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package id.co.id.rcs.http.server;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import id.co.id.rcs.utils.MyUtils;


/**
 *
 * @author mikomkz
 */
public class SocketServer {
    public static void main(String[] args) {
        int port = 8092;

        try {
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Server is listening on port " + port);

            while (true) {
                Socket socket = serverSocket.accept();

                final String request = new MyUtils().getString(socket.getInputStream());

                System.out.println("New client connected");

                OutputStream output   = socket.getOutputStream();
                PrintWriter  writer   = new PrintWriter(output, true);
                final String response = "{ \"DT\": \"20200709211353\", \"RC\": \"0000\", \"Notifikasi\": { \"c\": \"BJBPOIN\", \"u\": \"ib.bankbjb.co.id\", \"m\": \"Hutan PDAM & Jabar telah menerima pembayaran sebesar Rp. 9,930 tanggal 09/07/2020 21:13:47, status transaksi: SUKSES dengan No Ref: 200709050055. Saldo Anda Rp. 90,393,496,225.\", \"n\": \"6281320684377\" } }ÿ";

                writer.println(response);
                output.close();
            }
        } catch (IOException ex) {
            System.out.println("Server exception: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}
